import cv2


def detect_face(image_path) -> None:
	# Loading the Haar Cascade Frontal Face Dictionnary
	face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
	# Loading the image entry
	image_object = cv2.imread(image_path)
	# Define the neural image filters here B&W filter is the best
	gray_filter = cv2.cvtColor(image_object, cv2.COLOR_BGR2GRAY)
	# Running...
	faces = face_cascade.detectMultiScale(gray_filter, 1.1, 4)

	# Draw
	for (x_axis, y_axis, width, height) in faces:
		cv2.rectangle(image_object, (x_axis, y_axis), (x_axis + width, y_axis + height), (255, 0, 0), 3)

	# Display
	cv2.imshow('Face detection result', image_object)
	cv2.waitKey()


if '__main__' == __name__:
	detect_face('test2.png')
