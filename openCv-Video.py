import cv2


def video_monitoring(video_path) -> None:
	face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
	# Read the input video
	video_capture = cv2.VideoCapture(video_path)

	while video_capture.isOpened():
		_, image = video_capture.read()

		# filter & detection
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		faces = face_cascade.detectMultiScale(gray, 1.1, 4)

		# draw
		for (x_axis, y_axis, width, height) in faces:
			cv2.rectangle(image, (x_axis, y_axis), (x_axis + width, y_axis + height), (255, 0, 0), 3)

		# Display the output
		cv2.imshow('Face detection system', image)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

	video_capture.release()


if '__main__' == __name__:
	video_monitoring('test.mp4')
