"""
BASIC PERCEPTRON IMPLEMENTATION
Returns 1|0 based on threshold condition
"""

# Initializations
x_entries = [0.1, 0.5, 0.2]
w_weights = [0.4, 0.3, 0.6]
threshold = 0.5


# Exit layer function
def step(weighted_sum) -> int:
	if threshold < weighted_sum:
		return 1

	return 0


# Layer sweeping
def perceptron() -> int:
	weighted_sum = 0

	for x, w in zip(x_entries, w_weights):
		weighted_sum += x * w
		print(f'For x={x} & w={w}, weighted_sum={weighted_sum}')

	return step(weighted_sum)


# Main
if '__main__' == __name__:
	output = perceptron()
	print(f'\nThe output : {output}')
